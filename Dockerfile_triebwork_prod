### Build and install packages
FROM python:3.8 as build-python

COPY requirements.txt /app/
WORKDIR /app


RUN pip install -r requirements.txt
RUN pip install elastic-apm

FROM tiangolo/uvicorn-gunicorn:python3.8-slim

RUN groupadd -r saleor && useradd -r -g saleor saleor

COPY --from=build-python /usr/local/lib/python3.8/site-packages/ /usr/local/lib/python3.8/site-packages/
COPY --from=build-python /usr/local/bin/ /usr/local/bin/

RUN apt-get update \
  && apt-get install -y \
  libxml2 \
  libssl1.1 \
  libcairo2 \
  libmagic1 \
  libpango-1.0-0 \
  libpangocairo-1.0-0 \
  libgdk-pixbuf2.0-0 \
  shared-mime-info \
  mime-support \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ENV APP_MODULE saleor.asgi:application
ENV PORT 8000
ENV KEEP_ALIVE 20
COPY . /app