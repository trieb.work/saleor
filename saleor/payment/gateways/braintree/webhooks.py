
from django.core.handlers.wsgi import WSGIRequest
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    QueryDict,
)

from . import (
    get_braintree_gateway
)
from ....core.transactions import transaction_with_commit_on_errors

@transaction_with_commit_on_errors()
def handle_webhook(request: WSGIRequest, gateway_config: "GatewayConfig"):
    gateway = get_braintree_gateway(**gateway_config.connection_params)

    webhook_notification = gateway.webhook_notification.parse(str(request.POST['bt_signature']), request.POST['bt_payload'])

    print(webhook_notification.kind)
    if webhook_notification.kind.LocalPaymentCompleted:
        print("Received Webhook for Local Payment Completed")


    return HttpResponse("[accepted]")